﻿using TylerCrawford.RocketLab.RocketManager.Data;

namespace TylerCrawford.RocketLab.RocketManager.Tests.Helpers
{
    internal static class RocketsDbHelper
    {
        public static RocketsContext RocketsContext = new();

        public static async Task InitializeDbAsync()
        {
            await RocketsContext!.Database.EnsureCreatedAsync();
            if (!RocketsContext.RocketNodes.Any())
                await RocketsContextSeed.SeedAsync(RocketsContext);
        }
    }
}
