using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using TylerCrawford.RocketLab.RocketManager.Controllers;
using TylerCrawford.RocketLab.RocketManager.Data;
using TylerCrawford.RocketLab.RocketManager.Models;
using TylerCrawford.RocketLab.RocketManager.Repositories.Interfaces;
using TylerCrawford.RocketLab.RocketManager.Services.Interfaces;
using TylerCrawford.RocketLab.RocketManager.Tests.Helpers;

namespace TylerCrawford.RocketLab.RocketManager.Tests;

public class RocketsControllerTests
{
    private readonly RocketsContext _rocketsContext;
    private readonly Mock<IRocketsRepository> _rocketsRepository;
    private readonly Mock<IRocketsService> _rocketsService;
    public RocketsControllerTests()
    {
        var tsk = RocketsDbHelper.InitializeDbAsync();
        tsk.Wait();
        _rocketsContext = RocketsDbHelper.RocketsContext;
        _rocketsRepository = new Mock<IRocketsRepository>();
        _rocketsService = new Mock<IRocketsService>();
    }

    [Fact]
    public void RocketsController_GetRocketNodes_ReturnsOk()
    {
        // Arrange
        _rocketsService.Setup(s => s.GetRocketNodes()).Returns(_rocketsContext.RocketNodes);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = controller.GetRocketNodes();
        var okResult = actionResult as OkObjectResult;

        // Assert
        Assert.NotNull(okResult);
        Assert.Equal(200, okResult.StatusCode);
    }

    [Fact]
    public async Task RocketsController_GetRocketNodeByPath_ReturnsOk()
    {
        // Arrange
        var path = "/Rocket1/Stage1/Engine1";
        var node = await _rocketsContext.RocketNodes.FirstOrDefaultAsync(x => x.Name == "Engine1");
        _rocketsService.Setup(s => s.GetRocketNodeByPath(path)).Returns(node);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = controller.GetRocketNodeByPath(path);
        var okResult = actionResult as OkObjectResult;

        // Assert
        Assert.NotNull(okResult);
        Assert.Equal(200, okResult.StatusCode);
    }

    [Fact]
    public async Task RocketsController_GetRocketNodeByPath_ReturnsNotFound()
    {
        // Arrange
        var path = "/Rocket1/Stage1/Engine4";
        var node = await _rocketsContext.RocketNodes.FirstOrDefaultAsync(x => x.Name == "Engine4");
        _rocketsService.Setup(s => s.GetRocketNodeByPath(path)).Returns(node);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = controller.GetRocketNodeByPath(path);
        var notFoundResult = actionResult as NotFoundObjectResult;

        // Assert
        Assert.NotNull(notFoundResult);
        Assert.Equal(404, notFoundResult.StatusCode);
    }

    [Fact]
    public async Task RocketsController_AddRocketNodeToParent_ReturnsOk()
    {
        // Arrange
        var nodeToAdd = new RocketNode("Test")
        {
            Id = Guid.NewGuid()
        };
        var parentNode = await _rocketsContext.RocketNodes.FirstOrDefaultAsync();
        _rocketsService.Setup(s => s.AddRocketNodeAsync(parentNode!.Id, nodeToAdd)).ReturnsAsync(nodeToAdd);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = await controller.AddRocketNodeAsync(parentNode!.Id, nodeToAdd);
        var okResult = actionResult as OkObjectResult;

        // Assert
        Assert.NotNull(okResult);
        Assert.Equal(200, okResult.StatusCode);
    }

    [Fact]
    public async Task RocketsController_AddRocketNodeToParent_ReturnsNotFound()
    {
        // Arrange
        var nodeToAdd = new RocketNode("Test")
        {
            Id = Guid.NewGuid()
        };
        var fakeId = Guid.NewGuid();
        _rocketsService.Setup(s => s.AddRocketNodeAsync(fakeId, nodeToAdd)).Returns(null);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = await controller.AddRocketNodeAsync(fakeId, nodeToAdd);
        var notFoundResult = actionResult as NotFoundObjectResult;

        // Assert
        Assert.NotNull(notFoundResult);
        Assert.Equal(404, notFoundResult.StatusCode);
    }

    [Fact]
    public async Task RocketsController_AddRocketNodeProperties_ReturnsOk()
    {
        // Arrange
        var properties = new Dictionary<string, decimal>
        {
            { "Test1", 13.00m },
            { "Test2", 66.230m }
        };

        var targetNode = await _rocketsContext.RocketNodes.FirstOrDefaultAsync();
        foreach (var prop in properties)
            targetNode!.Properties.Add(prop.Key, prop.Value);

        _rocketsService.Setup(s => s.AddRocketNodePropertiesAsync(targetNode!.Id, properties)).ReturnsAsync(targetNode!);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = await controller.AddRocketNodePropertiesAsync(targetNode!.Id, properties);
        var okResult = actionResult as OkObjectResult;

        // Assert
        Assert.NotNull(okResult);
        Assert.Equal(200, okResult.StatusCode);
    }

    [Fact]
    public async Task RocketsController_AddRocketNodeProperties_ReturnsNotFound()
    {
        // Arrange
        var properties = new Dictionary<string, decimal>
        {
            { "Test1", 13.00m },
            { "Test2", 66.230m }
        };

        var targetNode = await _rocketsContext.RocketNodes.FirstOrDefaultAsync();
        foreach (var prop in properties)
            targetNode!.Properties.Add(prop.Key, prop.Value);

        _rocketsService.Setup(s => s.AddRocketNodePropertiesAsync(targetNode!.Id, properties)).Returns(null);
        var controller = new RocketsController(_rocketsService.Object);

        // Act
        IActionResult actionResult = await controller.AddRocketNodePropertiesAsync(targetNode!.Id, properties);
        var notFoundResult = actionResult as NotFoundObjectResult;

        // Assert
        Assert.NotNull(notFoundResult);
        Assert.Equal(404, notFoundResult.StatusCode);
    }
}