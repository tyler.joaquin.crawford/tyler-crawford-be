﻿using Microsoft.EntityFrameworkCore;
using TylerCrawford.RocketLab.RocketManager.Data;
using TylerCrawford.RocketLab.RocketManager.Models;
using TylerCrawford.RocketLab.RocketManager.Repositories.Interfaces;

namespace TylerCrawford.RocketLab.RocketManager.Repositories
{
    public class RocketsRepository : IRocketsRepository
    {
        private readonly RocketsContext _rocketContext;

        public RocketsRepository(RocketsContext rocketContext)
        {
            _rocketContext = rocketContext;
        }

        public IEnumerable<RocketNode> Get()
        {
            /* * 
             * Fetch with tree structure (as opposed to a flattened result).
             * ref: https://stackoverflow.com/questions/41827918/loading-a-full-hierarchy-from-a-self-referencing-table-with-entityframework-core
             */
            return _rocketContext.RocketNodes
                .Include(r => r.Children)
                .ToList()
                .Where(r => r.Parent == null);
        }

        public RocketNode? GetById(Guid id)
        {
            var q = _rocketContext.RocketNodes
                .Include(r => r.Children)
                .ToList()
                .FirstOrDefault(r => r.Id == id);
            return q;
        }

        public RocketNode? GetByName(string name)
        {
            return _rocketContext.RocketNodes
                .Include(r => r.Children)
                .ToList()
                .FirstOrDefault(r => r.Name == name);
        }

        public async Task SaveChangesAsync() => await _rocketContext.SaveChangesAsync();

        public async Task UpdateAsync(RocketNode node)
        {
            _rocketContext.Update(node); // I'm sure there's a better way to force tracking on converted EF properties than this.
            await _rocketContext.SaveChangesAsync();
        }
    }
}
