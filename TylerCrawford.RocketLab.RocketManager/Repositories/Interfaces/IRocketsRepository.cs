﻿using TylerCrawford.RocketLab.RocketManager.Models;

namespace TylerCrawford.RocketLab.RocketManager.Repositories.Interfaces
{
    public interface IRocketsRepository
    {
        /// <summary>
        /// Fetches a hierarchical collection of nodes.
        /// </summary>
        IEnumerable<RocketNode> Get();

        /// <summary>
        /// Fetches a node by ID.
        /// </summary>
        /// <param name="id">The ID of the node.</param>
        RocketNode? GetById(Guid id);

        /// <summary>
        /// Fetches a node by name.
        /// </summary>
        /// <param name="name">The name of the node.</param>
        RocketNode? GetByName(string name);

        /// <summary>
        /// Saves context changes to the database.
        /// </summary>
        Task SaveChangesAsync();

        /// <summary>
        /// Updates tracking for the specified entity and saves context changes to the database.
        /// </summary>
        /// <param name="node">The entity to update.</param>
        Task UpdateAsync(RocketNode node);
    }
}
