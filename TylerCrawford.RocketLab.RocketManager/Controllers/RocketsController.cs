using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using TylerCrawford.RocketLab.RocketManager.Models;
using TylerCrawford.RocketLab.RocketManager.Services.Interfaces;

namespace TylerCrawford.RocketLab.RocketManager.Controllers;

[ApiController]
[Route("api/rockets/nodes")]
public class RocketsController : ControllerBase
{
    private readonly IRocketsService _rocketsService;

    public RocketsController(IRocketsService rocketsService)
    {
        _rocketsService = rocketsService;
    }

    /// <summary>
    /// Fetch all rocket nodes.
    /// </summary>
    /// <returns>A collection of <see cref="RocketNode"/>.</returns>
    [HttpGet, Route("")]
    public IActionResult GetRocketNodes()
    {
        IEnumerable<RocketNode> rockets;

        try
        {
            rockets = _rocketsService.GetRocketNodes();
        }
        catch (Exception)
        {
            return BadRequest(ModelState);
        }

        return Ok(rockets);
    }

    /// <summary>
    /// Fetch a rocket node by id.
    /// </summary>
    /// <param name="id">The ID of the rocket node to fetch.</param>
    /// <returns>A <see cref="RocketNode"/>.</returns>
    [HttpGet, Route("{id}")]
    public IActionResult GetRocketNode([Required] Guid id)
    {
        RocketNode? rocketNode;

        try
        {
            rocketNode = _rocketsService.GetRocketNode(id);
            if (rocketNode == null) return NotFound("The requested rocket node was not found.");
        }
        catch (Exception)
        {
            return BadRequest(ModelState);
        }

        return Ok(rocketNode);
    }

    [HttpGet, Route("path")]
    public IActionResult GetRocketNodeByPath([Required][FromQuery] string path)
    {
        RocketNode? rocketNode;

        try
        {
            // Validate the path pattern
            var match = Regex.IsMatch(path, "^(\\/([a-zA-Z\\d])+)+");
            if (!string.IsNullOrEmpty(path) && !Regex.IsMatch(path, "(?:\\/[a-zA-Z\\d]+)"))
                return BadRequest($"Required parameter {nameof(path)} must be a valid path (example: '\\rocket\\module\\submodule').");

            rocketNode = _rocketsService.GetRocketNodeByPath(path);

            if (rocketNode == null) return NotFound("The requested rocket node was not found.");
        }
        catch (Exception)
        {
            return BadRequest(ModelState);
        }

        return Ok(rocketNode);
    }

    /// <summary>
    /// Add a new node to an existing rocket node.
    /// </summary>
    /// <param name="id">The ID of the rocket node to add to.</param>
    /// <param name="node">The rocket node object to add.</param>
    /// <returns>The <see cref="RocketNode"/> that was created (with its DB-assigned ID).</returns>
    [HttpPut, Route("{id}")]
    public async Task<IActionResult> AddRocketNodeAsync([Required] Guid id, [FromBody] RocketNode node)
    {
        if (!ModelState.IsValid) return BadRequest();

        RocketNode? rocketNode;
        try
        {
            rocketNode = await _rocketsService.AddRocketNodeAsync(id, node);
            if (rocketNode == null) return NotFound("The requested rocket node to modify was not found.");
        }
        catch (Exception)
        {
            return BadRequest(ModelState);
        }

        return Ok(rocketNode);
    }

    /// <summary>
    /// Adds properties to an existing rocket node.
    /// </summary>
    /// <param name="id">The ID of the rocket node to add to.</param>
    /// <param name="properties">A collection of properties to add to the target rocket node.</param>
    /// <returns>The <see cref="RocketNode"/> that was modified.</returns>
    [HttpPut, Route("{id}/properties")]
    public async Task<IActionResult> AddRocketNodePropertiesAsync(Guid id, [FromBody] Dictionary<string, decimal> properties)
    {
        if (!ModelState.IsValid) return BadRequest();

        RocketNode? rocketNode = null;
        try
        {
            rocketNode = await _rocketsService.AddRocketNodePropertiesAsync(id, properties);
            if (rocketNode == null) return NotFound("The requested rocket node to modify was not found.");
        }
        catch (Exception)
        {
            BadRequest(ModelState);
        }

        return Ok(rocketNode);
    }
}
