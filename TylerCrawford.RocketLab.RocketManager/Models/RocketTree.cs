﻿using System.ComponentModel.DataAnnotations;

namespace TylerCrawford.RocketLab.RocketManager.Models
{
    /// <summary>
    /// A generic base class for representing a RocketLab non-binary tree structure.
    /// </summary>
    /// <typeparam name="T">The node type of the tree.</typeparam>
    public abstract class RocketLabTree<T> where T : RocketLabTree<T>
    {
        private List<T> _children = new();
        private Guid? _parentId = null;

        /// <summary>
        /// The primary identifier for the current tree node.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The <see cref= "T" /> node's parent node.
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore]
        public T? Parent { get; set; } = null;

        /// <summary>
        /// The ID of the current node's parent.
        /// </summary>
        /// <remarks>This was more or less added just so Entity Framework could have a foreign key association.</remarks>
        [System.Text.Json.Serialization.JsonIgnore]
        public Guid? ParentId
        {
            get => _parentId;
            set
            {
                if (this.Parent != null)
                    _parentId = this.Parent.Id;
            }
        }

        /// <summary>
        /// A collection of <see cref="T" /> children nodes
        /// </summary>
        public List<T> Children
        {
            get => _children;
            set
            {
                foreach (var child in value)
                    child.Parent ??= this as T;
                _children = value;
            }
        }

        /// <summary>
        /// Returns true if the current <see cref="T"/> node is the root of the non-binary tree.
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore]
        public bool IsRoot { get => this.Parent == null; }

        /// <summary>
        /// Returns true if the current <see cref="T"/> node is a leaf in the non-binary tree.
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore]
        public bool IsLeaf { get => _children == null || _children.Count == 0; }

        /// <summary>
        /// Returns the current node's level away from the root node.
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore]
        public int Level { get => IsRoot ? 0 : (Parent?.Level ?? 0) + 1; }

        /// <summary>
        /// Indexer for tree node of <see cref="T" />.
        /// </summary>
        /// <param name="name">The name for the <see cref="T" /> node.</param>
        /// <returns>A tree node of <see cref="T" />.</returns>
        [System.Text.Json.Serialization.JsonIgnore]
        public abstract T this[string name] { get; }

        /// <summary>
        /// Indexer for tree node of <see cref="T" />.
        /// </summary>
        /// <param name="id">The identifier or name for the <see cref="T" /> node.</param>
        /// <returns>A tree node of <see cref="T" />.</returns>
        [System.Text.Json.Serialization.JsonIgnore]
        public T this[Guid id]
        {
            get
            {
                if (id == this.Id) return (T)this;
                return this.Children.First(c => c.Id == id);
            }
        }

        /// <summary>
        /// Adds a child node to the current node's children collection.
        /// </summary>
        /// <param name="node"></param>
        /// <remarks>Adds the node to the end of the children collection.</remarks>
        /// <returns>The newly added <see cref="T"/> node.</returns>
        public T Add(T node)
        {
            if (node.Parent != null) node.Parent.Children.Remove(node);

            node.Parent = this as T;

            _children.Add(node);

            return node;
        }

        /// <summary>
        /// Traverse the <see cref="RocketLabTree{T}"/> tree (by "First In, Last Out" order).
        /// </summary>
        public IEnumerable<T> Traverse()
        {
            var queue = new Queue<RocketLabTree<T>>();

            queue.Enqueue(this);

            while (queue.Count > 0)
            {
                var current = queue.Dequeue();

                yield return (T)current;

                if (current.Children != null)
                    foreach (var child in current.Children)
                        queue.Enqueue(child);
            }
        }

        /// <summary>
        /// Fetches a <see cref="T"/> node via an identifier.
        /// </summary>
        /// <param name="id">The identifier of the node to fetch.</param>
        public abstract T? Find(string id);
    }
}
