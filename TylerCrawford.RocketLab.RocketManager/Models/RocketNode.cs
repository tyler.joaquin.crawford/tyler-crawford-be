﻿using System.ComponentModel.DataAnnotations;

namespace TylerCrawford.RocketLab.RocketManager.Models
{
    /// <summary>
    /// A non-binary tree node that represents a rocket module.<br/><br/>
    /// Child nodes are associated rocket modules and may contain properties.
    /// </summary>
    public sealed class RocketNode : RocketLabTree<RocketNode>
    {
        /// <summary>
        /// Creates a new <see cref="RocketNode" />.
        /// </summary>
        /// <param name="name">The name (and unique key) for the <see cref="RocketNode" />.</param>
        /// <remarks>Note: the <see cref="Name" /> value is required.</remarks>
        public RocketNode(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// The name for the <see cref="RocketNode" />.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// A collection of a <see cref="RocketNode" />'s properties.
        /// </summary>
        public Dictionary<string, decimal> Properties { get; set; } = new()!;

        /// <summary>
        /// The time of creation.
        /// </summary>
        public DateTime? Created { get; set; }

        /// <summary>
        /// The time of modification.
        /// </summary>
        public DateTime? Modified { get; set; } = null;

        /// <summary>
        /// Indexer for <see cref="RocketNode" />.
        /// </summary>
        /// <param name="name">The name (and unique key) for the <see cref="RocketNode" />.</param>
        /// <returns>A <see cref="RocketNode" />.</returns>
        [System.Text.Json.Serialization.JsonIgnore]
        public override RocketNode? this[string name]
        {
            get
            {
                if (name == this?.Name) return this;
                return this.Children?.FirstOrDefault(c => c.Name == name) ?? null;
            }
        }

        /// <summary>
        /// Fetches a <see cref="RocketNode"/> node by name.
        /// </summary>
        /// <param name="name">The name of the node to fetch.</param>
        public override RocketNode? Find(string name) => Traverse().FirstOrDefault(n => n.Name == name);
    }
}
