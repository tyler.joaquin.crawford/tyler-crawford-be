﻿using TylerCrawford.RocketLab.RocketManager.Models;

namespace TylerCrawford.RocketLab.RocketManager.Services.Interfaces
{
    /// <summary>
    /// The rockets service contract.
    /// </summary>
    public interface IRocketsService
    {
        /// <summary>
        /// Fetches all rocket nodes in a flat collection.
        /// </summary>
        IEnumerable<RocketNode> GetRocketNodes();

        /// <summary>
        /// Fetches a specific rocket node by ID.
        /// </summary>
        /// <param name="id">The rocket node ID.</param>
        RocketNode? GetRocketNode(Guid id);

        /// <summary>
        /// Fetches a specific rocket node by path.
        /// </summary>
        /// <param name="path">The node name path.</param>
        RocketNode? GetRocketNodeByPath(string path);

        /// <summary>
        /// Adds a rocket node to an existing node (parent).
        /// </summary>
        /// <param name="id">The ID of the parent-to-be rocket node.</param>
        /// <param name="node">The <see cref="RocketNode"/> object to add.</param>
        /// <remarks>Note: adds to the end of the rocket root's children.</remarks>
        ValueTask<RocketNode?> AddRocketNodeAsync(Guid id, RocketNode node);

        /// <summary>
        /// Assigns one or more properties to an existing rocket node.
        /// </summary>
        /// <param name="id">The ID of the rocket node to add to.</param>
        /// <param name="properties">The collection of properties to add to the target rocket node.</param>
        ValueTask<RocketNode?> AddRocketNodePropertiesAsync(Guid id, Dictionary<string, decimal> properties);
    }
}
