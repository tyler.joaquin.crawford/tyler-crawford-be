﻿using TylerCrawford.RocketLab.RocketManager.Models;
using TylerCrawford.RocketLab.RocketManager.Repositories.Interfaces;
using TylerCrawford.RocketLab.RocketManager.Services.Interfaces;

namespace TylerCrawford.RocketLab.RocketManager.Services
{
    public class RocketsService : IRocketsService
    {
        private readonly IRocketsRepository _rocketsRepository;

        public RocketsService(IRocketsRepository rocketsRepository)
        {
            _rocketsRepository = rocketsRepository;
        }

        public IEnumerable<RocketNode> GetRocketNodes() => _rocketsRepository.Get();

        public RocketNode? GetRocketNode(Guid nodeId) => _rocketsRepository.GetById(nodeId);

        public RocketNode? GetRocketNodeByPath(string path)
        {
            var names = path.Split("/", StringSplitOptions.RemoveEmptyEntries);
            if (!names.Any()) return null;

            var nodes = _rocketsRepository.Get();

            /* *
             * Iterates through the parsed node path names and 
             * uses the accessor helper method to traverse 
             * the tree.
             */
            RocketNode? node = null;
            for (int i = 0; i < names.Length; i++)
            {
                var nodeName = names[i];
                if (i == 0)
                {
                    // Fetch first node - we'll pull following nodes from it's children (if any)
                    node = nodes.FirstOrDefault(r => r.Name == nodeName);
                    if (node == null) break;
                    continue;
                }
                node = node![nodeName];

                // We couldn't find the requested node
                if (node == null) break;
            }
            return node;
        }

        public async ValueTask<RocketNode?> AddRocketNodeAsync(Guid id, RocketNode node)
        {
            var target = _rocketsRepository.GetById(id);

            if (target == null) return null;

            // Add the new node to the target / parent node
            target.Add(node);

            // Update the entity in the database
            await _rocketsRepository.SaveChangesAsync();

            // Return the added node with the database-assigned ID
            return _rocketsRepository.Get().First(r => r.Id == node.Id);
        }

        public async ValueTask<RocketNode?> AddRocketNodePropertiesAsync(Guid id, Dictionary<string, decimal> properties)
        {
            var target = _rocketsRepository.GetById(id);

            if (target == null) return null;

            // Add new properties or replace existing property values
            foreach (var prop in properties)
                target.Properties.Add(prop.Key, prop.Value);

            // Update the entity in the database
            await _rocketsRepository.UpdateAsync(target);

            return target;
        }
    }
}
