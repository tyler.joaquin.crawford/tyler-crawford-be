﻿using TylerCrawford.RocketLab.RocketManager.Models;

namespace TylerCrawford.RocketLab.RocketManager.Data
{
    public class RocketsContextSeed
    {
        /// <summary>
        /// Seed the in-memory database with a few rocket objects.
        /// </summary>
        /// <param name="context">The rockets context.</param>
        public static async Task SeedAsync(RocketsContext context)
        {
            if (!context.RocketNodes.Any())
            {
                // Buld a collection of nodes with semi-random values.
                var rockets = new List<RocketNode>() {

                    // ROCKET 1
                    new RocketNode("Rocket1")
                    {
                        Properties = new Dictionary<string, decimal>()
                        {
                            { "Height", 18.00m },
                            { "Mass", 12000.000m },
                        },
                        Children = new List<RocketNode>()
                        {
                            new RocketNode("Stage1")
                            {
                                Children = new List<RocketNode>()
                                {
                                    new RocketNode("Engine1")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 9.493m },
                                            { "ISP", 12.156m },
                                        },
                                    },
                                    new RocketNode("Engine2")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 9.413m },
                                            { "ISP", 11.632m },
                                        },
                                    },
                                    new RocketNode("Engine3")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 9.899m },
                                            { "ISP", 12.551m },
                                        },
                                    }
                                }
                            },
                            new RocketNode("Stage2")
                            {
                                Children = new List<RocketNode>()
                                {
                                    new RocketNode("Engine1")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 1.622m },
                                            { "ISP", 12.156m },
                                        },
                                    },
                                }
                            }
                        }
                    },

                    // ROCKET 2
                    new RocketNode("Rocket2")
                    {
                        Properties = new Dictionary<string, decimal>()
                        {
                            { "Height", 15.40m },
                            { "Mass", 11603.000m },
                        },
                        Children = new List<RocketNode>()
                        {
                            new RocketNode("Stage1")
                            {
                                Children = new List<RocketNode>()
                                {
                                    new RocketNode("Engine1")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 8.883m },
                                            { "ISP", 11.411m },
                                        },
                                    },
                                    new RocketNode("Engine2")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 8.541m },
                                            { "ISP", 11.020m },
                                        },
                                    },
                                }
                            }
                        }
                    },

                    // ROCKET 3
                    new RocketNode("Rocket3")
                    {
                        Properties = new Dictionary<string, decimal>()
                        {
                            { "Height", 15.40m },
                            { "Mass", 11422.000m },
                        },
                        Children = new List<RocketNode>()
                        {
                            new RocketNode("Stage1")
                            {
                                Children = new List<RocketNode>()
                                {
                                    new RocketNode("Engine1")
                                    {
                                        Properties = new Dictionary<string, decimal>()
                                        {
                                            { "Thrust", 8.622m },
                                            { "ISP", 11.103m },
                                        },
                                    },
                                }
                            }
                        }
                    }
                };

                await context.AddRangeAsync(rockets);
                await context.SaveChangesAsync();
            }
        }
    }
}
