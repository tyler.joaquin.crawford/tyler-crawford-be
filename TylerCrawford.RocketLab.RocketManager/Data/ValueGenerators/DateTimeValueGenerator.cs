﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace TylerCrawford.RocketLab.RocketManager.Data.ValueGenerators
{
    public class DateTimeValueGenerator : ValueGenerator<DateTime>
    {
        public override bool GeneratesTemporaryValues { get; }
        public override DateTime Next(EntityEntry entry) => DateTime.UtcNow;
    }
}
