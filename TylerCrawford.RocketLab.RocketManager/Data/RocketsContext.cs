﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using System.Text.Json;
using TylerCrawford.RocketLab.RocketManager.Data.ValueGenerators;
using TylerCrawford.RocketLab.RocketManager.Models;

namespace TylerCrawford.RocketLab.RocketManager.Data
{
    /// <summary>
    /// The in-memory EF data context for RocketsDb.
    /// </summary>
    public class RocketsContext : DbContext
    {
        private readonly IConfiguration? _configuration;

        public RocketsContext()
        {

        }

        public RocketsContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("RocketsDb");
        }

        /// <summary>
        /// The rocket nodes database collection.
        /// </summary>
        public DbSet<RocketNode> RocketNodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<RocketNode>(builder =>
            {
                builder.HasIndex(new string[2] { "Id", "Name" }).IsUnique();
                builder.HasKey(e => e.Id);
                builder.Property(e => e.Id).HasValueGenerator<SequentialGuidValueGenerator>().IsRequired();
                builder.Property(e => e.Name).IsRequired();
                builder.Property(e => e.Created).ValueGeneratedOnAdd().HasValueGenerator<DateTimeValueGenerator>();
                builder.Property(e => e.Modified).ValueGeneratedOnUpdate().HasValueGenerator<DateTimeValueGenerator>();

                /* * 
                 * We need to spin our own conversion of Dictionary as EF does not natively support dictionaries.
                 * ref: https://learn.microsoft.com/en-us/ef/core/modeling/value-conversions?tabs=data-annotations#configuring-a-value-converter
                 */
                builder.Property(p => p.Properties)
                    .HasConversion(
                        v => JsonSerializer.Serialize(v, new JsonSerializerOptions()),
                        v => JsonSerializer.Deserialize<Dictionary<string, decimal>>(v, new JsonSerializerOptions())); // Not sure why Null Ref warning triggers when the member is decorated with null-forgiving operator...

                /* *
                 * Declare a relational structure for the hierarchical tree of RocketNode objects.
                 * ref: https://habr.com/en/post/516596/
                 */
                builder.HasOne(x => x.Parent)
                    .WithMany(x => x.Children)
                    .HasForeignKey(x => new { x.ParentId })
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}
