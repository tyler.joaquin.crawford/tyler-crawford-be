using Microsoft.EntityFrameworkCore;
using TylerCrawford.RocketLab.RocketManager.Data;
using TylerCrawford.RocketLab.RocketManager.Repositories;
using TylerCrawford.RocketLab.RocketManager.Repositories.Interfaces;
using TylerCrawford.RocketLab.RocketManager.Services;
using TylerCrawford.RocketLab.RocketManager.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

ConfigureServices(builder);

var app = builder.Build();

// Seed the in-memory Rockets database.
await ConfigureDatabase(app);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseCors("AllowOrigin");
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();


/* * 
 * Configure business logic / repository services.
 * 
 * Note: I prefer to contain these declarations in their own method for readability
 * as an application grows in scope.
 */
void ConfigureServices(WebApplicationBuilder builder)
{
    builder.Services.AddCors(options =>
    {
        options.AddPolicy(
            name: "AllowOrigin",
            builder => {
                builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
            });
    });
    builder.Services.AddScoped<IRocketsRepository, RocketsRepository>();
    builder.Services.AddScoped<IRocketsService, RocketsService>();
    builder.Services.AddDbContext<RocketsContext>(options => options.UseInMemoryDatabase(databaseName: "RocketDb"));
}

/* * 
 * Creating a service scope to retrieve injected context and calling seed 
 * to push in our rocket objects.
 * ref: https://exceptionnotfound.net/ef-core-inmemory-asp-net-core-store-database/
 */
async Task ConfigureDatabase(WebApplication app)
{
    using var scope = app.Services.CreateScope();
    var rocketsContext = scope.ServiceProvider.GetRequiredService<RocketsContext>();
    await RocketsContextSeed.SeedAsync(rocketsContext);
}
