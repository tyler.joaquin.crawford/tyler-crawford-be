### Backend Development

#### Overview

This challenge will test the following skills:

- .NET environment
- Code Quality
  - Adherence to best practice
- REST APIs

Allow at least 3 hours to complete.

Do not be discouraged if you are unable to complete aspects of the challenge, it is designed to test all levels of ability.

#### Rules

- Complete the challenge(s) on your own.
- Referencing of online resources is expected
- All code, markup, and assets should be pushed to the provided repository.
- You are encouraged to ask us questions at any point.
- Note any deviations from the specification

#### Instructions

1.  Set up a .NET project
2.  Setup the following database model using Entity Framework Core using the in-memory database provider:

- A rocket (root node) is built from a tree of nodes.
- Each node has a name.
- Each node can have any number of properties.
  - A property is a key value pair, where the key is a string and the value is a decimal number.
- The path of a node can be inferred from the name hierarchy (e.g. _'/root/parent/child'_).

3.  Seed the database with the following structure (entries with values are properties, others are nodes):

- Rocket
  - Height: 18.000
  - Mass: 12000.000
  - Stage1
    - Engine1
      - Thrust: 9.493
      - ISP: 12.156
    - Engine2
      - Thrust: 9.413
      - ISP: 11.632
    - Engine3
      - Thrust: 9.899
      - ISP: 12.551
  - Stage2
    - Engine1
      - Thrust: 1.622
      - ISP: 15.110

4.  Expose HTTP REST endpoints for the following operations:

    1.  Create a node with a specified parent
    2.  Add a property on a specific node
    3.  Return the subtree of nodes with their properties for a provided node path

5.  Create unit tests for your solution.
